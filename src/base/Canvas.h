//
// Created by Nilupul Sandeepa on 2021-05-28.
//

#ifndef CANVAS_H
#define CANVAS_H

#include <functional>
#include <unordered_set>

#include "../utils/Macros.h"
#include "../utils/Types.h"
#include "../utils/Definitions.h"
#include "../utils/BlendUtils.h"
#include "Camera.h"
#include "Layer.h"
#include "../../../../../libs/openFrameworks/gl/ofFbo.h"
#include "../../../../../libs/openFrameworks/graphics/ofGraphics.h"
#include "../../../../../libs/openFrameworks/graphics/ofImage.h"
#include "../../../../../libs/openFrameworks/math/ofMath.h"

NS_ADE_BEGIN

class Canvas {
    public:
        Canvas(int width, int height);
        void setup();
        void update();
        void draw();

        //Callbacks
        void setCanvasCallback(ChangeCallback callback);


    protected:
        ChangeCallback canvasChangeCallback;
        ColorCallback colorPickerCallback;

        int width;
        int height;
        int visibleWidth;
        int visibleHeight;

        ofFbo sharedBuffer;

        CanvasMode canvasMode;
        CanvasMode previousCanvasMode;

        bool shouldLoadPaintings;
        bool shouldShowBackground;
        bool shouldShowOverlay;
        bool needLayerReloading;
        bool isColorPickerEnabled;
        bool isParticlesEnabled;
        bool isPreUndoStepAdded;
        bool isIpadProHack;
        int changesStarted;
        bool isFrameEnabled;
        bool isMagicEnabled;
        int numberOfSimultaneousGestures;
        float imageOpacity;
        float tempImageOpacity;

        //UndoManager undoManager;
        //SaveUtils saveUtils;
        ofImage* background;
        Camera* camera;

        //Entity currentTouched;

        std::vector<Layer*> layersArray;
        std::unordered_set<int> needSaving;

};

NS_ADE_END

#endif //CANVAS_H