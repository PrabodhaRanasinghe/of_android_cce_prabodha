package com.axis.cce;

import android.opengl.GLSurfaceView;
import android.util.Log;

import cc.openframeworks.OFActivity;
import cc.openframeworks.OFAndroidLifeCycle;

public class CCEApp {

    private static native void nInit();
    public static void init() {
        ((GLSurfaceView) OFAndroidLifeCycle.mGLView).queueEvent(new Runnable() {
            @Override
            public void run() {
                CCEApp.nInit();
            }
        });
    }

    public static void onCanvasChange(int state) {
        Log.i("CCEApp", "OnCanvasChange : " + state);
    }
}
